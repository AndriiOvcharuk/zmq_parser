<?php
class Database {
    private $conn;

    function __construct() {
    }

    function connect() {

        // Connecting to mysql database
        //$this->conn = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME, 3306);
        try {
            $this->conn = new \PDO('mysql:host='.$_ENV['DB_HOST'].';dbname='.$_ENV['DB_NAME'], $_ENV['DB_USER'], $_ENV['DB_PASSWORD']);
        } catch(PDOException $e) {
            echo "You have an error: ".$e->getMessage()."<br>";
            echo "On line: ".$e->getLine();
        }

        // returing connection resource
        return $this->conn;
    }
}