<?php
require_once("vendor/autoload.php");

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

require_once("lib/db.php");
require_once("lib/colors.php");

use Sunra\PhpSimple\HtmlDomParser;
use Carbon\Carbon;

$colors = new Colors();



define('WORKERS_SOCK', 'ip_workers');
define('WEBSITE_URL', 'http://www.uefa.com/');
define('WORKERS_COUNT', 8);



function parse_team_data($teamLink){
    $db = new Database();
    $colors = new Colors();

    //  Socket to send messages to
    $context = new ZMQContext();
    $sender = new ZMQSocket($context, ZMQ::SOCKET_PUSH);
    $sender->connect("ipc://sink.ipc");

    $dbh = $db->connect();
    //$teamPage = HtmlDomParser::file_get_html(WEBSITE_URL.$teamLink->href);
    $teamPage = HtmlDomParser::file_get_html(WEBSITE_URL.$teamLink);
    $teamNameH1 = $teamPage->find('h1[class=team-name]');
    $team_name = $teamNameH1[0]->plaintext;
    $playerPagesLinks = $teamPage->find('a[class=squad--player-img]');

    //echo $colors->getColoredString("$team_name parcing start...", "yellow").PHP_EOL;
    $sender->send($colors->getColoredString("$team_name parcing start...", "yellow"));
    $stmt = $dbh->prepare("INSERT INTO players (full_name, country, position, birthday, age, caps, goals, club, height, weight)
                            VALUES (:full_name, :country, :position, :birthday, :age, :caps, :goals, :club, :height, :weight)");

    $stmt->bindParam(':full_name', $full_name);
    $stmt->bindParam(':country', $country);
    $stmt->bindParam(':position', $position);
    $stmt->bindParam(':birthday', $birthday);
    $stmt->bindParam(':age', $age);
    $stmt->bindParam(':caps', $caps);
    $stmt->bindParam(':goals', $goals);
    $stmt->bindParam(':club', $club);
    $stmt->bindParam(':height', $height);
    $stmt->bindParam(':weight', $weight);

    foreach ($playerPagesLinks as $playerPagesLink){
        $playerPage = HtmlDomParser::file_get_html(WEBSITE_URL.$playerPagesLink->href);
        $playerAttributes = $playerPage->find('span[class=profile--list--data]');

        if(!isset($playerAttributes[0]))
            continue;
        $full_name = $playerAttributes[0]->plaintext;
        $country = $playerAttributes[1]->plaintext;
        $position = $playerAttributes[2]->plaintext;
        $birthday  = Carbon::createFromFormat('d/m/Y', explode(' ',$playerAttributes[3]->plaintext)[0])->toDateString();
        $age  = preg_replace("/[^0-9,.]/", "", explode(' ',$playerAttributes[3]->plaintext)[1]);
        $caps = $playerAttributes[4]->plaintext;
        $goals = $playerAttributes[5]->plaintext;
        $club = $playerAttributes[8]->plaintext;
        $height = $playerAttributes[9]->plaintext;
        $weight = $playerAttributes[10]->plaintext;

        $stmt->execute();

        //echo $playerAttributes[0]->plaintext."| ".$playerAttributes[1]->plaintext."| ".$playerAttributes[2]->plaintext."| ".$birthday." | ".$age."| ".$playerAttributes[4]->plaintext."| ".$playerAttributes[5]->plaintext."| ".$playerAttributes[8]->plaintext."| ".$playerAttributes[9]->plaintext."| ".$playerAttributes[10]->plaintext."| "."\n";
    }
    //echo $colors->getColoredString("$team_name parcing end...", "green").PHP_EOL;
    $sender->send($colors->getColoredString("$team_name parcing end...", "green"));
}

function worker_routine()
{
    $context = new ZMQContext();
    //  Socket to receive messages on
    $receiver = new ZMQSocket($context, ZMQ::SOCKET_PULL);
    $receiver->connect("ipc://ventilator.ipc");

    while(true){
        $message = $receiver->recv();
        if($message){
            if($message == 'stop'){
                break;
            }
            else{
                parse_team_data($message);
            }
        }
    }
}

function prepare_workers()
{
    for ($threadNbr = 0; $threadNbr < WORKERS_COUNT; $threadNbr++) {
        $pid = pcntl_fork();
        if ($pid == 0) {
            worker_routine();
            exit();
        }
    }
}

function prepare_team_links()
{
    $teamsCatalog = HtmlDomParser::file_get_html(WEBSITE_URL.'/uefaeuro/season=2016/teams/index.html/');
    $teamLinksObjects = $teamsCatalog->find('a[class=team-hub_link]');
    $teamLinks = array();

    foreach ($teamLinksObjects as $linksObject){
        $teamLinks[] = $linksObject->href;
    }

    return $teamLinks;
}


//Get links to uefa teams pages
$teamLinks = array_slice(prepare_team_links(), 22);
//$teamLinks = prepare_team_links();




//Dialog with user
echo $colors->getColoredString(count($teamLinks).' team'.(count($teamLinks) == 1 ? '' : 's').' will be parsed! Continue (y/n): ', 'blue');
$fp = fopen('php://stdin', 'r');
$inputMsg = trim(strtolower(fgets($fp, 512)));
fclose($fp);
switch ($inputMsg){
    case 'y':
        break;
    default:
        exit();
}

//Start workers
prepare_workers();

$context = new ZMQContext();
//  Socket to send messages on
$sender = new ZMQSocket($context, ZMQ::SOCKET_PUSH);
$sender->bind("ipc://ventilator.ipc");

echo $colors->getColoredString('Sending tasks to workers...', 'blue').PHP_EOL;

//  The first message is "0" and signals start of batch
$sender->send(0);

//  Send  workload to workers (links of teams)
for ($i = 0; $i < count($teamLinks); $i++) {
    $sender->send($teamLinks[$i]);
}

//Pull socket to get messages from workers
$context = new ZMQContext();
$receiver = new ZMQSocket($context, ZMQ::SOCKET_PULL);
$receiver->bind("ipc://sink.ipc");

//Getting messages from workes
for ($task_nbr = 0; $task_nbr < count($teamLinks) * 2; $task_nbr++) {
    $string = $receiver->recv();
    echo $string.PHP_EOL;
}

//Stop all workers
for ($i = 0; $i < WORKERS_COUNT; $i++) {
    $sender->send('stop');
}

echo $colors->getColoredString('All team are successfully parsed! Press any key to exit', 'blue').PHP_EOL;


